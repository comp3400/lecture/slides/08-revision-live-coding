-- what is the type of?
-- `\f x -> pure f <*> x`

data List a = Nil | Cons a (List a)
data Function a b = Function (a -> b)
data Optional a = Empty | Full a

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons h t) = f h `Cons` fmap f t
instance Functor Optional where
  fmap _ Empty = Empty
  fmap f (Full a) = Full (f a)
instance Functor (Function a) where
  fmap f (Function g) =
    Function (f . g)

-- write these instances
instance Applicative List where
  pure a = Cons a Nil
  Nil <*> _ = Nil
  Cons h t <*> x =
    let app Nil y = y
        app (Cons h t) y = Cons h (t `app` y)
    in fmap h x `app` (t <*> x)
instance Applicative Optional where
  pure = Full
  Empty <*> _ = Empty
  _ <*> Empty = Empty
  Full f <*> Full a = Full (f a)
instance Applicative (Function a) where
  pure = Function . pure
  Function f <*> Function a = Function (f <*> a)

lift2 f a b = f <$> a <*> b

-- complete then experiment with these derived operations
leftApply ::
  Applicative k => k b -> k a -> k b
leftApply = lift2 const

rightApply ::
  Applicative k => k a -> k b -> k b
rightApply = lift2 (const id)

swizzle ::
  Applicative k => List (k x) -> k (List x)
swizzle Nil = pure Nil
swizzle (Cons h t) = lift2 Cons h (swizzle t)
