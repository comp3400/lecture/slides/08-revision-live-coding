import Prelude hiding (Ordering, sum, (++), compare)

data Optional a = Empty | Full a

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional func optl =
  case optl of
    Empty -> Empty
    Full x -> Full (func x)

-- Practice by writing this example function:
bindOptional :: (a -> Optional b) -> Optional a -> Optional b
bindOptional =
  \f o ->
    case o of
      Empty -> Empty
      Full a -> f a

data List a = Nil | Cons a (List a)

-- adds up the numbers in a list
sum :: List Integer -> Integer
sum Nil = 0
sum (Cons h t) = h + sum t
-- appends two lists
(++) :: List x -> List x -> List x
Nil ++ y = y
Cons h t ++ y = Cons h (t ++ y)
-- flattens a list of lists
flatten :: List (List x) -> List x
flatten Nil = Nil
flatten (Cons h t) = h ++ flatten t
-- reverses a list
reverse :: List x -> List x
reverse =
  let reverse0 acc Nil = acc
      reverse0 acc (Cons h t) = reverse0 (Cons h acc) t
  in  reverse0 Nil

data Or a b = IsA a | IsB b

modifyA :: (a -> a) -> Or a b -> Or a b
modifyA f (IsA a) = IsA (f a)
modifyA _ (IsB b) = IsB b

getB :: Or a b -> Optional b
getB (IsA _) = Empty
getB (IsB b) = Full b

data Tree a = Tree a (List (Tree a))

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f (Tree r x) =
  let mapList _ Nil = Nil
      mapList f (Cons h t) = f h `Cons` mapList f t
  in  Tree (f r) (mapList (mapTree f) x)

traverseTreeOptional ::
  (a -> Optional b) -> Tree a -> Optional (Tree b)
traverseTreeOptional f (Tree r x) =
  let lift2Optional _ Empty _ = Empty
      lift2Optional _ _ Empty = Empty
      lift2Optional k (Full a) (Full b) = Full (k a b)
      traverseList _ Nil = Full Nil
      traverseList f (Cons h t) = lift2Optional Cons (f h) (traverseList f t)
  in  lift2Optional Tree (f r) (traverseList (traverseTreeOptional f) x)

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

-- the error message
parseError :: ParseResult x -> Optional String
parseError (ParseError e) = Full e
parseError (ParseSuccess _ _) = Empty
-- return a parser that succeeds, puts the input to the output
neutralParser :: x -> Parser x
neutralParser = \x -> Parser (ParseSuccess x)
-- a parser that consumes one character of input (if it can)
character :: Parser Char
character = Parser (\i ->
  case i of
    [] ->
      ParseError "Unexpected EOF"
    h:t ->
      ParseSuccess h t
  )

data Ordering = LessThan | EqualTo | GreaterThan deriving Eq
class Eq x => Order x where
  compare :: x -> x -> Ordering

data Vector6 a = Vector6 a a a a a a

maximumV6 :: Order a => Vector6 a -> a
maximumV6 =
  let mx q r = if q `compare` r == GreaterThan then q else r
  in  \(Vector6 a1 a2 a3 a4 a5 a6) -> mx a1 (mx a2 (mx a3 (mx a4 (mx a5 a6))))

reverseV6 :: Order a => Vector6 a -> Vector6 a
reverseV6 = \(Vector6 a1 a2 a3 a4 a5 a6) -> Vector6 a6 a5 a4 a3 a2 a1

mapList ::     (a -> b) -> List     a -> List     b
mapList _ Nil = Nil
mapList f (Cons h t) = f h `Cons` mapList f t
mapOptional' :: (a -> b) -> Optional a -> Optional b
mapOptional' _ Empty = Empty
mapOptional' f (Full a) = Full (f a)
mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapParser f (Parser g) =
  let mapParseResult _ (ParseError e) = ParseError e
      mapParseResult k (ParseSuccess a i) = ParseSuccess (k a) i
  in  Parser (mapParseResult f . g)
mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
mapVector6 f (Vector6 a1 a2 a3 a4 a5 a6) = Vector6 (f a1) (f a2) (f a3) (f a4) (f a5) (f a6)
