{-
-- Examine this expression
-- What is its type?
-- Keeping in mind:
--   (>>=) :: k a -> (a -> k b) -> k b
--   pure :: a -> k a
exercise0 :: ?
exercise0 =
  \f a ->
    f >>= \ff ->
    a >>= \aa ->
    pure (ff aa)
-}
-- write a function of this type
exercise1 ::
  Applicative k => (a -> b -> c) -> k a -> k b -> k c
exercise1 f a b = f <$> a <*> b
-- write a function of this type
exercise2 ::
  Monad k => (a -> k b) -> (b -> k c) -> (a -> k c)
exercise2 f g = \a -> f a >>= g

data Optional a = Empty | Full a
data List a = Nil | Cons a (List a)
data Function a b = Function (a -> b)

instance Functor Optional where
  fmap _ Empty = Empty
  fmap f (Full a) = Full (f a)
instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons h t) = f h `Cons` fmap f t
instance Functor (Function a) where
  fmap f (Function g) = Function (f . g)

instance Applicative Optional where
  pure = Full
  Empty <*> _ = Empty
  _ <*> Empty = Empty
  Full f <*> Full a = Full (f a)
instance Applicative List where
  pure a = Cons a Nil
  Nil <*> _ = Nil
  Cons h t <*> x =
    let app Nil y = y
        app (Cons h t) y = Cons h (t `app` y)
    in fmap h x `app` (t <*> x)
instance Applicative (Function a) where
  pure = Function . pure
  Function f <*> Function a = Function (f <*> a)


instance Monad Optional where -- exercise3
  return = Full
  Empty >>= _ = Empty
  Full a >>= f = f a

instance Monad List where -- exercise4
  return a = Cons a Nil
  Nil >>= _ = Nil
  Cons h t >>= f = 
    let app Nil y = y
        app (Cons h t) y = Cons h (t `app` y)
    in  f h `app` (t >>= f)

instance Monad (Function a) where -- exercise5
  return = Function . const
  Function x >>= g =
    let unFunction (Function r) = r
    in  Function (\a -> unFunction (g (x a)) a)

data FlipFunction a b = FlipFunction (b -> a)

-- exercise6
-- this will not be possible
-- try it to see why
instance Functor (FlipFunction a) where
  fmap = error "todo: instance Functor (FlipFunction a)"

data FromIntOrBool a =
  FromInt (Int -> a)
  | FromBool (Bool -> a)

instance Functor FromIntOrBool where
  fmap f (FromInt g) =
    FromInt (f . g)
  fmap f (FromBool g) =
    FromBool (f . g)

-- exercise7
-- this will not be possible
-- try it to see why
instance Applicative FromIntOrBool where
  pure = error "todo: instance Applicative FromIntOrBool#pure"
  (<*>) = error "todo: instance Applicative FromIntOrBool#(<*>)"

-- exercise8
flatten :: Monad k => k (k a) -> k a
flatten = (=<<) id

-- exercise9
-- only when True return the given argument
onlyWhen :: Monad f => Bool -> f () -> f ()
onlyWhen p x = if p then x else pure ()

type Input = String
  
data ParseResult a =
  Error String
  | Success a Input
  deriving Show
  
newtype Parser a =
  Parser (Input -> ParseResult a)
  
parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
 
  
-- exercise10
instance Functor ParseResult where
  fmap _ (Error e) = Error e
  fmap f (Success a i) = Success (f a) i

-- exercise11
instance Functor Parser where
  fmap f (Parser k) = Parser (fmap f . k)
-- exercise12
instance Applicative Parser where
  pure a = Parser (Success a)
  Parser f <*> Parser a =
    Parser (\i ->
      case f i of
        Error s ->
          Error s
        Success g j ->
          fmap g (a j)
    )

-- exercise13
instance Monad Parser where
  return =
    pure
  Parser g >>= f =
    Parser (\i ->
      case g i of
        Error s ->
          Error s
        Success a j ->
          parse (f a) j
    )

-- exercise14
-- fails a parser with the given error message
failed :: String -> Parser a
failed = Parser . pure . Error

-- exercise15
-- parse one character, or fail if no character available
character :: Parser Char
character =
  Parser (\i ->
    case i of
      [] ->
        Error "EOF"
      (c:r) ->
        Success c r
  )

-- exercise16
-- parse one character that satisfies the given function
-- if the character does not satisfy the given function,
--   fail with the given error message
satisfy :: String -> (Char -> Bool) -> Parser Char
satisfy e p =
  character >>= \c ->
  if p c
    then
      pure c
    else
      failed e

-- test1: Error "hello"
test1 = (+1) <$> Error "hello"
-- test2: Success 100 "bye"
test2 = (+1) <$> Success 99 "bye"
-- test3: Error "message"
test3 = parse (failed "message") "input"
-- test4: Success 'i' "nput"
test4 = parse character "input"
-- test5: Error "EOF"
test5 = parse character ""
-- test6: Success 'i' "nput"
test6 = parse (satisfy "is i" (=='i')) "input"
-- test7: Error "is x"
test7 = parse (satisfy "is x" (=='x')) "input"
-- test8: Error "EOF"
test8 = parse (satisfy "is x" (=='x')) ""
-- test9: Success ('i','n') "put"
test9 =
  parse (
    character >>= \c1 ->
    character >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test10: Success ('i','n') "put"
test10 =
  parse (
    character >>= \c1 ->
    satisfy "is n" (== 'n') >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test11: Error "is x"
test11 =
  parse (
    character >>= \c1 ->
    satisfy "is x" (== 'x') >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test12: Error "EOF"
test12 =
  parse (
    character >>= \c1 ->
    satisfy "is x" (== 'x') >>= \c2 ->
    pure (c1, c2)
  ) "i"
