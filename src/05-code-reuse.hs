data List a = Nil | Cons a (List a)
instance Functor List where 
  fmap _ Nil = Nil
  fmap f (Cons h t) = f h `Cons` fmap f t
data Pair a b = Pair a b
instance Functor (Pair a) where
  fmap f (Pair a b) = Pair a (f b)
data Tree a = Node a [Tree a]
instance Functor Tree where
  fmap f (Node r x) = Node (f r) (fmap (fmap f) x)
data Function a b = Function (a -> b)
instance Functor (Function a) where
  fmap f (Function g) = Function (f . g)
data Vector6 a = Vector6 a a a a a a
instance Functor Vector6 where
  fmap f (Vector6 a1 a2 a3 a4 a5 a6) = Vector6 (f a1) (f a2) (f a3) (f a4) (f a5) (f a6)

-- Why or why not can you write instance Functor
-- Try it!
data FlippedFunction a b = FlippedFunction (b -> a)
instance Functor (FlippedFunction a) where
  fmap = error "todo: instance Functor (FlippedFunction a)"
