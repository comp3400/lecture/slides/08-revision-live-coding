#!/usr/bin/env bash

set -e

if [ $# -lt 2 ]
  then
    echo "Supply arguments: CI_PROJECT_URL CI_COMMIT_SHA"
    exit 1
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=etc
index=${etc_dir}/index.html

mkdir -p ${dist_dir}

sed "s~{CI_PROJECT_URL}~${1}~g; s~{CI_COMMIT_SHA}~${2}~g" ${index} > ${dist_dir}/index.html
